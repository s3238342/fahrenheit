package nl.utwente.di.convertor;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;


public class FullConvertor extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Convertor convertor;
	
    public void init() throws ServletException {
    	convertor = new Convertor();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Celsius to Fahrenheit";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>celsius number: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Degree: " +
                   convertor.convertToFahr(Double.parseDouble(request.getParameter("celsius"))) +
                "</BODY></HTML>");
  }
}
