package nl.utwente.di.convertor;

public class Convertor {
    public double convertToFahr(double cel){
        return (cel*((double) 9 /5))+32;
    }
}
