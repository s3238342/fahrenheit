package nl.utwente.di.convertorTests;

import nl.utwente.di.convertor.Convertor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class TestConvertor {
    @Test
    public void testConverting() throws Exception {
        Convertor convertor = new Convertor();
        double price = convertor.convertToFahr(10);
        Assertions.assertEquals(50,price);
    }
}
